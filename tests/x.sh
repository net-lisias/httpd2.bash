#!/usr/bin/env bash
dir="/xxx/yyy/zzz.html"

if [[ $dir =~ ^/.* ]]; then
	echo "Match /"
fi

parse_url() {
	local query1 query2 path1 path2
	local proto url login host port resource path query fragment
	declare -A URL

	# extract the protocol
	proto="$(echo $1 | grep :// | sed -e's,^\(.*://\).*,\1,g')"

	if [[ ! -z $proto ]] ; then
		# remove the protocol
		url="$(echo ${1/$proto/})"

		# extract the user (if any)
		login="$(echo $url | grep @ | cut -d@ -f1)"

		# extract the host
		host="$(echo ${url/$login@/} | cut -d/ -f1)"

		# by request - try to extract the port
		port="$(echo $host | sed -e 's,^.*:,:,g' -e 's,.*:\([0-9]*\).*,\1,g' -e 's,[^0-9],,g')"

		# extract the uri (if any)
		resource="/$(echo $url | grep / | cut -d/ -f2-)"
	else
		url=""
		login=""
		host=""
		port=""
		resource=$1
	fi

	# extract the path (if any)
	path1="$(echo $resource | grep ? | cut -d? -f1 )"
	path2="$(echo $resource | grep \# | cut -d# -f1 )"
	path=$path1
	if [[ -z $path ]] ; then path=$path2 ; fi
	if [[ -z $path ]] ; then path=$resource ; fi

	# extract the query (if any)
	query1="$(echo $resource | grep ? | cut -d? -f2-)"
	query2="$(echo $query1 | grep \# | cut -d\# -f1 )"
	query=$query2
	if [[ -z $query ]] ; then query=$query1 ; fi

	# extract the fragment (if any)
	fragment="$(echo $resource | grep \# | cut -d\# -f2 )"

	URL["proto"]=$proto
	URL["url"]=$url
	URL[login]=$login
	URL[host]=$host
	URL[port]=$port
	URL[resource]=$resource
	URL[path]=$path
	URL[query]=$query
	URL[fragment]=$fragment
}

print_url() {
	echo ${URL["proto"]}${URL["url"]}
	echo "   proto: ${URL[proto]}"
	echo "   login: ${URL[login]}"
	echo "    host: ${URL[host]}"
	echo "    port:	${URL[port]}"
	echo "resource:	${URL[resource]}"
	echo "    path: ${URL[path]}"
	echo "   query: ${URL[query]}"
	echo "fragment: ${URL[fragment]}"
}

parse_url "http://login:password@example.com:8080/one/more/dir/file.exe?a=sth&b=sth#anchor_fragment"
print_url
parse_url "https://example.com/one/more/dir/file.exe#anchor_fragment"
print_url
parse_url "http://login:password@example.com:8080/one/more/dir/file.exe#anchor_fragment"
print_url
parse_url "ftp://user@example.com:8080/one/more/dir/file.exe?a=sth&b=sth"
print_url
parse_url "/one/more/dir/file.exe"
print_url
parse_url "file.exe"
print_url
parse_url "file.exe#anchor"
print_url
